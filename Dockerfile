FROM openjdk:8u111-jdk-alpine
EXPOSE 8080
VOLUME /tmp
ADD /build/libs/mateo-calculadora-basica-0.0.1.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]